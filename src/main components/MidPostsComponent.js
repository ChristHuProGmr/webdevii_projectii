import { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import thumbsup from '../thumbsup.png';
import garbage from '../garbage.PNG';
let incrementer = [];

const MidPostsComponent = (props)=>{

let [reRender, setRerender] = useState(false);


/**
 * increment or decrement the like counter of the post
 * Author: Maxime with help from Christopher and Vinh
 * @param {object} post 
 * @param {number} number 
 */
function incrementLikes(post, number){

    let id = post.id;
    let temps = document.querySelectorAll('.temp')

    let ind = 0;
    for(let k of temps){
        if(k.id == id){
            k.textContent = parseInt(k.textContent) + number;
        }
        ind++;
    }
        
}

/**
 * upon click the garbage bin, the method will remove the post.
 * Author: Maxime with help from Christopher and Vinh
 * @param {object} post 
 */
function removePost(post){
    let id = post.id;
    let temps = document.querySelectorAll('.temp')

    let listPost = props.form[props.currentCat].topicList[props.currentTopic].listPosts;

    let ind = 0;
    for(let k of temps){
        if(k.id == id){
            listPost.splice(ind, 1);
        }
        ind++;
    }
    setRerender(!reRender);
}


    useEffect(()=>{
        let listPosts = props.form[props.currentCat].topicList[props.currentTopic].listPosts;
        ReactDOM.render(
            <section id='postCollection'>
               { listPosts.map((post)=>{
                   return   <section key={post.id} className='postContainer'>
                                <div className="post">
                                    <span>{post.text}</span>
                                    <button className='like' id='like'><img src={thumbsup} width='40px' alt='thumbsup'
                                    onClick={()=>incrementLikes(post, 1)}></img></button>
                                    <button className='like'id='thumbsdown'><img src={thumbsup} width='40px' alt='dislike' onClick={()=>incrementLikes(post, -1)}></img></button>
                                </div>
                                <div className="poster">
                                    <span>by: {post.author} </span>
                                    <span>  {post.date}  </span>
                                   likes:  <span id={post.id} className = 'temp'> {post.likes}</span> <button onClick = {()=>removePost(post)} className='garbage_btn'><img className='garbage' src={garbage} alt = 'remove'></img></button>
                                </div>
                            </section>;
               })
               }
            </section>

            , document.getElementById('mid-forum-component')
        )
    }, [props, incrementLikes,removePost, reRender])

    return(
        <section id="mid-forum-component">

        </section>
    );
}

export default MidPostsComponent;