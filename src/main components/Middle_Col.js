import { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import MidPostsComponent from "./MidPostsComponent";

const Middle_Col = (props)=>{

    /**
     * folling states help track indexes of Category and Topic arrays
     * -Christopher Hu
     */
    let [currentCat, setCurrentCat] = useState(0);
    let [currentTopic, setCurrentTopic] = useState(0);

    //Triggered with submit post button to force re-render.
    let [forceEffect,setForceEffect] = useState(false);

    /**
     * initializes select options from prop data
     * -christopher hu
     */
    useEffect(()=>
        ReactDOM.render(
            <div id="selectOptions">
                <h4>Category: </h4>
                <select name="category" id="category">
                    {props.form.map((cat)=>{
                        return  <option value={cat.name} key={cat.id} onClick={()=>{changeCat(cat.id)}}>
                                    {cat.name}
                                </option>;
                    })}
                </select>
                <h4>Topic: </h4>
                <select name="topic" id="topic">
                    {props.form[currentCat].topicList.map((topic)=>{
                        return  <option value={topic.topic_title} key={topic.id} onClick={()=>{changeTop(topic.id)}}>
                                    {topic.topic_title}
                                </option>;
                    })}
                </select>
                
            </div>
            , document.getElementById('mid-cat-component')
        )
        // Dependencies
        , [currentCat, currentTopic, forceEffect]
    );  
    
    /**
     * function changes the state of currentCat which acts as an index
     * for the category array
     * Since id starts at 1 for the first entry, we must subtract 1 to get the index for the array
     * -christopher hu
     * @param {number} elem is category id
     */
    function changeCat(elem){
        //prevents crashing (out of index like error because category 2 only has one topic)
        //when switching to category 2 when topic index is >= than 1
        if(document.getElementById("category").value === "Category 2 - Project Management " && currentTopic >= 1){
            setCurrentTopic(0);
            setCurrentCat(elem - 1);
        }
        else{
            setCurrentCat(elem - 1);
        }
    }

    /**
     * function changes the state of currentTopic which acts as an index
     * for the topic array
     * Since id starts at 1 for the first entry, we must subtract 1 to get the index for the array
     * -christopher hu
     * @param {number} elem is category id
     */
    function changeTop(elem){
        setCurrentTopic(elem - 1);
    }

    /**
     * called when clicking the "submit post" button (defined below)
     * it will create a new post, fill it with information
     * then append it to the current topic's array.
     * method will then force a re-render with setForceEffect()
     * -christopher hu
     */
    function submitOpinion(){
        let date = new Date();
        let month = date.getMonth() + 1;
        let minutes = date.getMinutes() < 10 ? '0'+ date.getMinutes() : date.getMinutes();
        let dateString = `${date.getFullYear()}-${month}-${date.getDate()} ${date.getHours()}:${minutes}`
        let targetTopic = (props.form[currentCat].topicList[currentTopic].listPosts);
        let yourOpinion = document.getElementById('yourOpinion').value;

        let newPost =   {   author: "anonymous", date: dateString, id: targetTopic.length+1,
                            likes: 0, parentId: targetTopic[0].parentId, rate: 0,
                            replies: 0, text: yourOpinion, topic_id: targetTopic[0].topic_id
                        }

        targetTopic.push(newPost);

        setForceEffect(!forceEffect);
    }

    return(
        <section id="mid-col">

            <section id='mid-cat-component'>

            </section>

        <   MidPostsComponent form={props.form} currentCat={currentCat} currentTopic={currentTopic}/>

            <section id="mid-submit-message">
                <input type="text" id='yourOpinion'></input>
                <button onClick={()=>submitOpinion()}>Submit Post</button>
            </section>
        </section>
        
    );
}

export default Middle_Col;