import TopicTable from "../table components/TopicTable";
import StatsTable from "../table components/StatsTable";
import RecentPostTable from "../table components/RecentPostTable";
/**
 * Author: Vinh Dat Hoang
 * Return the right column html elements and display topic ranking, 
 * display recent user posting  and display user total post
 * @param {Array} param0 // json data as an array 
 * @returns 
 */
const Right_Col = ({users, forum})=>{
    return(
        <section id="right-col">
            <section>
                <p className="right-col-sect-headers">Ranked Topics</p>
                
                {forum && <TopicTable forum={forum} />} 
				
            </section>

            <section>
                <p className="right-col-sect-headers">Recent Posts</p>
                {forum && <RecentPostTable forum={forum} />}
            </section>

            <section>
                <p className="right-col-sect-headers">Post Stats</p>
                {users && <StatsTable users={users} />}
            </section>
        </section>
    );
}

export default Right_Col;