import { useEffect, useState } from "react";
import TopicTable from "./TopicTable";
const RankTopic = () => {
    let [forum, setForum] = useState(null);

    useEffect(()=>{
    let url1 = "http://localhost:3001/categories";
        fetch(url1).then(resp=>{
            if(!resp.ok){throw new Error(`An error has occured ${resp.status}`);}
            return resp.json();
        }).then(data=>{
            setForum(data);
        }).catch(err=>{
            console.log(err);
        })
    }, []);

    return (
        <section >
            
            {forum && <TopicTable forum = {forum} />}
          
        </section>
    );
}

export default RankTopic;
