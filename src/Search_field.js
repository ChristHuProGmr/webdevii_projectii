/**
 * Maxime
 * checks if the posts match the user's input in the textfield. If they don't, hide those posts, otherwise unhide them.
 */
function checkCorrectness(){
let posts = document.querySelectorAll('.postContainer');
let spans = document.querySelectorAll('.post span');
let textField= document.querySelector("#seaarchfield");

let text = textField.value;
let incr = 0;
for(let k of spans){
         if (!k.textContent.includes(text)){
            posts[incr].style.display= 'none';
            incr++;
         }
         else{
             posts[incr].style.display='block';
             incr++;


         }
   }
}


const Search_field = () => {
return (<span><input type='text' id='seaarchfield' placeholder='search the entire column...'onKeyUp={()=> checkCorrectness()}></input></span>)
}
export default Search_field;