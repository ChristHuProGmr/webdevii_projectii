const Footer = ()=>{
    return(
        <section className="head-footer">
            <h2>Project 2 - How to React</h2>
            <p>Student 1: Christopher Hu - 1435688 | Student 2: Maxime Rayvich - 2032446 | Student 3: Vinh Dat Hoang -  0720203</p>
        </section>
    );
}

export default Footer;
