/**
 * Author: Vinh Dat Hoang
 * Return a table for topic ranking as html elements
 * @param {Array} param0 // forum json data as an array
 * @returns // return html elements representing the topic ranking table
 */
const TopicTable = ({ forum }) => {
    let list = []
    let count = 0;
  
     // create a new list that is only topicList data and sorted by highest number of post.
    function newList() {
     
        forum.forEach((elem) => {
             elem.topicList.forEach((elem) => { list.push(elem) }) 
            });

        list.sort(sorting);
       
        function sorting(a, b) {
            if (a.nberPost < b.nberPost || a.nberPost === b.nberPost)
                return 1
            else return -1;
        }

    }
   
    return (
        <section>
          
        <table className="statsTable">
            <thead>
                <tr>
                    <th>Title</th>
                    <th># of posts</th>
                    <th>Status</th>
                
                </tr>
            </thead> 
            <tbody>       
                {newList()}
                {
                    list.map((detail) => (
                        <tr key={count++}>
                            <td> {detail.topic_title}</td>
                            <td> {detail.nberPost}</td>
                            <td> {detail.status}</td>
                        </tr>
                    ))
                }
          </tbody>
            </table>
        </section>
    );
}

export default TopicTable;