/**
 * Author: Vinh Dat Hoang
 * Return a table for user total post as html elements
 * @param {Array} param0 // forum json data as an array
 * @returns // return html elements representing user total post table
 */
const StatsTable = ({ users }) => {

    let count = 0;

    return (
        <section>

            <table className="statsTable">
                <thead>
                    <tr>
                        <th>user</th>
                        <th>total posts</th>

                    </tr>
                </thead>
                <tbody>

                    {
                        // sort users data array by highest number of post then use map to display them in td elements
                        users.sort(
                            (a, b) => {
                                if (a.nberPosts < b.nberPosts || a.nberPosts === b.nberPosts)
                                    return 1
                                else return -1;
                            })
                            .map((detail) => (
                                <tr key={count++}>
                                    <td> {detail.user_id}</td>
                                    <td> {detail.nberPosts}</td>

                                </tr>
                            ))
                    }
                </tbody>
            </table>
        </section>
    );
}

export default StatsTable;