/**
 * Author: Vinh Dat Hoang
 * Return a table for recent post as html elements.  
 * @param {Array} param0  // forum json data as an array
 * @returns // return html elements representing the recent post table
 */
const RecentPostTable = ({ forum }) => {
    let list = []
    let count = 0;

    // create a new list that is only listPosts data and sorted by earliest date.
    function newList() {
        // create a new list for only listPosts data
        forum.forEach((elem) => {
            elem.topicList.forEach((elem) => {
                elem.listPosts.forEach((elem) => { list.push(elem) })
            })
        });
        // sort the new list
        list.sort(sorting);

        // sort by earliest date
        function sorting(a, b) {
            if (a.date < b.date || a.date === b.date)
                return 1
            else return -1;
        }

    }

    return (
        <section>

            <table className="statsTable">
                <thead>
                    <tr>
                        <th>Author</th>
                        <th>Rank</th>
                        <th>Last Post</th>

                    </tr>
                </thead>
                <tbody>
                    {newList()}
                    {

                        list.map((detail) => (
                            <tr key={count++}>
                                <td> {detail.author}</td>
                                <td> {detail.rate}</td>
                                <td> {detail.date}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </section>
    );
}


export default RecentPostTable;