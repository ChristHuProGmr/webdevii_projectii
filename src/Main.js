import Left_Col from "./main components/Left_Col";
import Right_Col from "./main components/Right_Col";
import Middle_Col from "./main components/Middle_Col";
import { useEffect, useState } from "react";


//npx json-server --watch data/forum.json --port 3002
//Now, I don't have to memorize the command!

const Main = () => {

    let [forum, setForum] = useState(null);
    let [users, setUsers] = useState(null);
    let setJson = [setForum, setUsers];

    /**
     * Fetching json data from server and convert them to an array
     * -Christopher & Vinh
     */
    useEffect(() => {
        let url1 = ["http://localhost:3001/categories", "http://localhost:3002/users"];
        for (let index = 0; index < url1.length; index++) {
            fetch(url1[index]).then(resp => {
                if (!resp.ok) { throw new Error(`An error has occured ${resp.status}`); }
                return resp.json();
            }).then(data => {
                setJson[index](data);
            }).catch(err => {
                console.log(err);
            })
        }
    }, []);

    return (
        <section className="main-sect">
            <Left_Col />
            {forum && <Middle_Col form={forum} />}
            {users && forum && <Right_Col users={users} forum={forum} />}
        </section>
    );
}

export default Main;