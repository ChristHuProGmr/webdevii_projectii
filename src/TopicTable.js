
const TopicTable = ({ forum }) => {
    let list = []
    let count = 0;
    function test() {
        let test = [];
        forum.map((elem) => { elem.topicList.map((i) => { test.push(i) }) });

        test.sort(sorting);

        console.log(test);
        list = test
        function sorting(a, b) {
            if (a.nberPost < b.nberPost || a.nberPost == b.nberPost)
                return 1
            else return -1;
        }

    }
    return (
        <table className="statsTable">
            <thead>
                <tr>
                    <th>Title</th>
                    <th># of posts</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {test()}
                {

                    list.map((detail) => (
                        <tr key={count++}>
                            <td> {detail.topic_title}</td>
                            <td> {detail.nberPost}</td>
                            <td> {detail.status}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    );
}

export default TopicTable;